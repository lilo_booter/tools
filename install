#!/usr/bin/env bash

die () { echo "ERROR: $*" ; exit 1 ; }
has () { command -v "$1" >/dev/null ; }
fullpath () { readlink -f "$1" ; }

# Move to the directory where the install script is located
cd "$( dirname "${BASH_SOURCE[0]}" )" || die "Unable to change directory"

# Try to recover the private info url if not specfied
private_url=$1
[[ "$private_url" == "" && -f .private ]] && private_url=$( < .private )

# If we have curl and the private_url is specified, then attempt to fetch
has curl && [ "$private_url" ] && {
	curl --fail --silent "$private_url/env" -o bash/env || die "Unable to obtain env from $private_url"
	curl --fail --silent "$private_url/network_map" -o bash/network_map || die "Unable to obtain network_map from $private_url"
	echo "$private_url" > .private
}

# Attempt to bootstrap - unavailable tools are reported
has_should_report=1 source "bash/bootstrap" | sort | uniq

# Update ~/.bashrc
rc="$HOME/.bashrc"
[ -f "$rc" ] || die "No $rc file."

bootstrap="$( fullpath bash/bootstrap )"
[ -f "$bootstrap" ] || die "No bootstrap founds at $bootstrap"

grep "$bootstrap" "$rc" > /dev/null 2>/dev/null || {

cat >> "$rc" << EOF

# Initialise local tools
source "$bootstrap"
EOF

}
