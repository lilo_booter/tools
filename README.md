# tools

General colletion of bash functions and tools.

## install

Currently the only way to install the software is by way of the install script -
note that this doesn't copy any files to the system and the vast majority will
just be referenced directly from the git checkout.

Simplest use is just:

```
./install
```

### missing packages

install will report tools which aren't found - feel free to install them from
your package manager or ignore if unavailable or not required.

After installation, you can run install again or just create a new login shell
to obtain the new functionality.

### custom files

The install step will also indicate if 'custom files' have been created - these
are user provided files which hold local/private information such as ip addresses.

Currently there are two such files which the user is free to create in place:

* `bash/env`
* `bash/network_map`

The expected content of these fies is described below.

### advanced

The install script takes a single argument to a url which can be accessed via
curl and should point to a directory which contains the 'custom files'.

This url is cached such that subsequent pulls and installs will not require the
argument and will update these files automatically.

## bash directory

The main content is in the bash directory - this explains the functionality provided
by each file.

### bootstrap

This file is the one which includes all the others - it is placed in your .bashrc
file by the install script as:

```
source /full/path/to/tools/bash/bootstrap
```

### aliases

Whilst most of the functionality here are bash functions, the aliases directory is used
to keep some useful definitions which are common on Ubuntu but lacking elsewhere.

Currently just defines aliases for grep which intoduce highlighting.

### env

This is one of the custom files mentioned above - if it doesn't exist, it is just ignored.

If you wish to create one, you can add any additional functions, aliases or
variable assignments as you want.

Specific additions are explained in place below.

### menu

This just provides a simple mechanism to convert bash functions of the form:

```
name.value [ args ]
```

to a simple menu using `dialog` - for example:

```
$ myfunc.hello () { echo "hello $@" ; }
$ myfunc.goodbye () { echo "goodbye $@" ; }
$ menu myfunc charlie
```

This will present a menu offering hello and goodbye, and selection will execute
the function, passing the additional arguments in.

Note the the current terminal is preserved when you exit the menu either by
selection or cancel.

This also defines a `menus` function which is simply defined as:

```
menus () { menu menu "$@" ; }
```

meaning that any subsequently defined `menu.name` functions will automatically be
shown when running the menus function.

Additionally, a collection of functions called `local.name` are created which
wrap potentially interesting command line tools such as btop, htop, mc, and others
when available.

Finally, `menu.local` is created.

### open

Adds `file-select` function.

### gnome

Sets up a script called `gnomessh` if running in an X environment (DISPLAY is
assigned) and the `gnome-terminal` is installed.

The `gnomessh` opens a ssh session in a new tab of an existing terminal or
creates a new one.

### network

The network functionality allows you to provide a list of network devices on your
LAN, VPN or on the internet which you have either wakeonlan or ssh access to.

In order to use this, you must create a local `bash/network\_map` file and it
has the following form:

```
# name [nic],[ssh]
machine xx:xx:xx:xx:xx:xx,ssh://user@machine:port
wakeable yy:yy:yy:yy:yy:yy
localhost none,ssh://localhost
```

In this case:

* `machine` can be woken up using the inet address, and accessed via ssh.
* `wakeable` will work for wakeonlan, but has no ssh info.
* `localhost` has no wakeolan and ssh info only.

This will reasult in a number of functions being defined:

* `ssh.name`
* `ssh-copy-id.name`
* `ssh-menus.name`
* `wake.name` (if wakeonlan is installed)

### vpn

Provides the ability to share an established VPN connection from the Linux
barracudavpn client (using `vpn.fwd`) with other Linux, Windows (by way of
cygwin) or OS/X systems (using `vpn.enable`). To reset the DNS after,
`vpn.disable` can be used.

In order for this to work, `env` should be updated with the following vars:

```
export VPN_OUTDEV="tun0"
export VPN_RANGES=( 10.0.0.0,255.0.0.0 )
export VPN_DNS=( 10.0.0.1 )
```

These should reflect the network you're connecting to of course.

Optionally, if you have one machine in your lan is commonly used to connect to
the vpn, you can add:

```
export VPN_SERVER=<address>
```

And this will be used as the default argument for `vpn.enable`.

I have no idea how `vpn.fwd` should work on any system other than Linux. Also,
for `vpn.enable` or `vpn.disable` to work on Cygwin, you will need elevated
admin rights, so the cygwin terminal should be opened appropriately. Linux and
OS/X assumes the current user is in the sudoers list, and will prompt for a
password when required.

### job

This is designed to be used with bash job control. The expected use
case is when you have multiple vim or nano editors open - ideally, one per
source file. Instead of exiting to access the shell, ctrl-z is used to
suspend the process - allowing a build and execution or other files to be
opened.

Running `job` will do the following:

* if only one job is running, this is brought to the foreground
* if multiple jobs are running, a menu is shown allowing selection or cancel

When a foregrounded process is again suspended, focus is brought back to the
shell.

When it is exited, the menu returns for the remaining processes. This is to
facilitate exiting multiple editors with the minimum of key presses.

Additionally, the following commands are provided:

* `job.cwd [pattern]` - show all jobs started in the current working directory
* `job.kill [pattern]` - kill all jobs by way of a term signal and foregrounding

### mkurl

This tool provides a means to share any file on your system as an http or https
url. It requires `nginx` to be installed (though presumably could be used with
other http servers).

It requires that you setup a home directory of the form `~/www` and this
directory will hold symbolic links to the shared files.

Running `mkurl --help` will provide more information.

### git

Possibly useless git commands which I tend to use:

* `git.modified [ dir ]` - lists modified files (within dir if specified)
* `git.add [ dir ]`- add modified files (within dir if specified)

### tmux

Provides various shortcuts for tmux commands:

* `tmux.rename` - renames the current tmux window
* `tmux.mouse [on|off]` - turns mouse on or off (defaults to on)

If tmux is not detected as running, the following is created:

* `local.tmux` - create or attach to a tmux session

### power

Provides functionality related to power management.

* `wakeup.devices` - linux only - provides a means to select which usb devices
wake the computer from suspend.
